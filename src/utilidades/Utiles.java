package utilidades;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import com.google.gson.Gson;

import mx.uv.opencvbitmap.modelo.Muestra;
import android.graphics.Bitmap;
import android.util.Base64;

public class Utiles {

	/**
	 * Funci�n que devuelve la fecha actual del sistema
	 * @return 
	 * String con la fecha en formato YYYY-MM-DD
	 */
	public String getFechaActual(){
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Funci�n que devuelve la hora actual del sistema 
	 * @return
	 * String con la hora en formato HH-MI-SS
	 */
	public String getHora(){
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.HOUR_OF_DAY) + "-" + calendar.get(Calendar.MINUTE) + "-" + calendar.get(Calendar.SECOND);
		
	}
	
	public String bitmap2Base64(Bitmap bitmap){
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
		byte[] byteArray = byteArrayOutputStream .toByteArray();	
		String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
		return encoded;
	}
	
	
	public String object2JSON(Muestra muestra){
		Gson gson = new Gson();
		return gson.toJson(muestra);
	}
	
	public double getLatitud(){
		
		return 0;
	}

	public double getLongitud(){
		
		return 0;
	}	
	
	
	
}
