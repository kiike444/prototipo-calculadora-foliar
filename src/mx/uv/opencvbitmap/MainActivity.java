package mx.uv.opencvbitmap;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
	private ImageView iv;
	private TextView tv;
	private TextView tvArea;
	private TextView tvClorofila;
	private ProgressBar pbarProgreso;
	private int formula_elegida = 0;
	private int imagen_elegida = 0;

	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
	        @Override
	        public void onManagerConnected(int status) {
	            if (status == LoaderCallbackInterface.SUCCESS ) {
	                // now we can call opencv code !
	            	//calcularArea();
	            } else {
	                super.onManagerConnected(status);
	            }
	        }
	    };	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		iv  = (ImageView)findViewById(R.id.ivImagen);
		//iv.setImageResource(R.drawable.prueba);
		tv = (TextView)findViewById(R.id.tvFormula);
		tvClorofila = (TextView)findViewById(R.id.tvClorofila);
		tvArea = (TextView)findViewById(R.id.tvArea);
		pbarProgreso = (ProgressBar)findViewById(R.id.progressBar);
	}
	
	
	@Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9,this, mLoaderCallback);
    }	
	
	public void clicBtnCalcularArea(View v){
		Bitmap bitmap;
		if (iv.getDrawable() instanceof BitmapDrawable) {
		    bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
		} else {
		    Drawable d = iv.getDrawable();
		    bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		    Canvas canvas = new Canvas(bitmap);
		    d.draw(canvas);
		}		
		
		new CalcularArea().execute(bitmap);
	}
	
	public void clicBtnCalcularClorofila(View v){
		final Bitmap bitmap;
		if (iv.getDrawable() instanceof BitmapDrawable) {
		    bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
		} else {
		    Drawable d = iv.getDrawable();
		    bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		    Canvas canvas = new Canvas(bitmap);
		    d.draw(canvas);
		}		
		new CalcularClorofila().execute(bitmap);
	}
	
	public void clicBtnCambiarImagen(View v){
		imagen_elegida++;
		
		if(imagen_elegida > 1){
			imagen_elegida=0;
		}
		
		switch(imagen_elegida){
		case 0:
			iv.setImageResource(R.drawable.hoja_verde_grande);
			break;
		case 1:
			iv.setImageResource(R.drawable.hoja_verde_pequena);
			break;
		case 2:
			iv.setImageResource(R.drawable.hoja_verde_grande_min);
			break;
		case 3:
			iv.setImageResource(R.drawable.hoja_verde_pequena_min);
			break;			
		};
		pbarProgreso.setProgress(0);
		tvArea.setText("�rea: ");
		tvClorofila.setText("Clorofila: ");

	}
	
	public void clicBtnCambiarFormula(View v){
		formula_elegida++;
		
		if(formula_elegida > 4){
			formula_elegida=0;
		}
		
		switch(formula_elegida){
		case 0:
			tv.setText("F�rmula 1: ((r+b)/255 - (r-b)/255) / 3 ");
			break;
		case 1:
			tv.setText("F�rmula 1: b/(r+b+g)");
			break;
		case 2:
			tv.setText("F�rmula 1: g/(r+g+b)");
			break;
		case 3:
			tv.setText("F�rmula 1: r/(r+g+b)");
			break;
		case 4:
			tv.setText("F�rmula 1: g/r");
			break;			
		};		
		
	}
	
	private class CalcularClorofila extends AsyncTask<Bitmap,Integer,Double>{

		@Override
		protected Double doInBackground(Bitmap... params) {
			double clorofila=0;
			double r=0;
			double g=0;
			double b=0;
			Mat imagen=new Mat();
			Utils.bitmapToMat(params[0], imagen);
			Imgproc.cvtColor(imagen, imagen, Imgproc.COLOR_BGR2RGB);
			double alturaImagen = imagen.size().height;
			double anchoImagen = imagen.size().width;
			publishProgress(10);
			//Imgproc.GaussianBlur(imagen, imagen, new Size(3, 3), 50);
			int count=0;
			//Sumamos los valores RGB de cada pixel de la imagen
			for(int i=0;i<alturaImagen;i++){
				for(int j=0;j<anchoImagen;j++){
					double pr = imagen.get(i, j)[0];
					double pg = imagen.get(i, j)[1];
					double pb = imagen.get(i, j)[2];
					if(! ( (pr == 255  && pg== 255 && pb == 255) || (pr == 0  && pg == 0 && pb == 0) ) ){
						//rojo
						if(pr!=0){
							r+=pr;
						}
						//verde
						if(pg!=0){
							g+=pg;
						}
						//azul
						if(pb !=0){
							b+=pb;
						}			
						count++;	
					}
					if(i==alturaImagen/3){
						publishProgress(30);
					}else{
						if(i==alturaImagen/2){
							publishProgress(50);
						}else{
							if(i==(alturaImagen/4)*3){
								publishProgress(80);
							}
						}
						
					}
				}
			}
			
			publishProgress(90);
			//Obtenemos un promedio en los tres canales de color (RGB) de la imagen
			r=r/count;
			g=g/count;
			b=b/count;
			
			//Aplicamos la f�rmula para calcular la clorofila
			switch (formula_elegida){
			case 0:
				clorofila = ( (((r+b)/255) - ((r-b)/255)) / 3 );
				break;
			case 1:
				clorofila = b/(r+b+g);
				break;
			case 2: 
				clorofila = g/(r+g+b);
				break;
			case 3:
				clorofila = r/(r+g+b);
				break;
			case 4:
				clorofila = g/r;
			}	
			
			publishProgress(100);
			
			return clorofila;
		}
		
	    @Override
	    protected void onProgressUpdate(Integer... values) {
	        int progreso = values[0].intValue();
	 
	        pbarProgreso.setProgress(progreso);
	    }		
		
	    @Override
	    protected void onPreExecute() {
	        pbarProgreso.setMax(100);
	        pbarProgreso.setProgress(0);
	    }
	 
	    @Override
	    protected void onPostExecute(Double result) {
	    	tvClorofila.setText("Clorofila = "+new DecimalFormat("#.##").format(result));
	    	pbarProgreso.setProgress(0);
	    }
	}		
		
	private class CalcularArea extends AsyncTask<Bitmap,Integer,Double>{

		@Override
		protected Double doInBackground(Bitmap... params) {
			Mat imagen=new Mat();
			Utils.bitmapToMat(params[0], imagen);
			Imgproc.cvtColor(imagen, imagen, Imgproc.COLOR_BGR2RGB);
			Imgproc.cvtColor(imagen, imagen, Imgproc.COLOR_RGB2GRAY);
			Imgproc.threshold(imagen, imagen, 60, 255, Imgproc.THRESH_BINARY_INV);
			double area=0;
			double alturaImagen = imagen.size().height;
			double anchoImagen = imagen.size().width;			
			double areaMarcador = alturaImagen/4;
			double progress50=imagen.size().height/2;
			double progress80=(imagen.size().height/4)*3;
			double pxref=0;
			double pxmuestra=0;
			publishProgress(10);

	    	for(int i=0;i<alturaImagen;i++){
	    		for(int j=0;j<anchoImagen;j++){
	    			double px = imagen.get(i, j)[0];
	    			if(i<=areaMarcador){
	    				if(px==255){
	    					pxref++;
	    				}
	    				if(areaMarcador == i){
	    					publishProgress(30);
	    				}
	    			}else{
	    				if(px==255){
	    					pxmuestra++;
	    				}	    				
	    			}
    				if(progress50 == i){
    					publishProgress(50);
    				}else{
        				if(progress80 == i){
        					publishProgress(80);
        				}
    				}	    			
	    		}
	    	}
	    		
	    	area = (pxmuestra/pxref);

			publishProgress(100);
			
			return area;
		}
		
	    @Override
	    protected void onProgressUpdate(Integer... values) {
	        int progreso = values[0].intValue();
	        pbarProgreso.setProgress(progreso);
	    }		
		
	    @Override
	    protected void onPreExecute() {
	        pbarProgreso.setMax(100);
	        pbarProgreso.setProgress(0);
	    }
	 
	    @Override
	    protected void onPostExecute(Double result) {
	    	tvArea.setText("�rea = "+new DecimalFormat("#.##").format(result)+" cm2");
	    	pbarProgreso.setProgress(0);
	    }
	}			
	
}//Termina main_activity

