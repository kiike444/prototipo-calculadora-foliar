package mx.uv.opencvbitmap.modelo;

public class Muestra {
	String foto="json string";
	private ResultadoArea resArea;
	private ResultadoClorofila resClorofila;
	
	public Muestra(){
		this.resArea=new ResultadoArea();
		this.resClorofila=new ResultadoClorofila();
	}
	
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public ResultadoArea getResArea() {
		return resArea;
	}
	public void setResArea(ResultadoArea resArea) {
		this.resArea = resArea;
	}
	public ResultadoClorofila getResClorofila() {
		return resClorofila;
	}
	public void setResClorofila(ResultadoClorofila resClorofila) {
		this.resClorofila = resClorofila;
	}
	

	
}
