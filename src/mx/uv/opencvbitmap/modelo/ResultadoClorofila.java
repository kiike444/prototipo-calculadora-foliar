package mx.uv.opencvbitmap.modelo;

public class ResultadoClorofila {
	private double clorofila=0;
	private String formula="x-y";
	private String fecha="2014-12-31";
	private String hora="12:30";
	private double latitud=0;
	private double longitud=0;
	
	
	public double getClorofila() {
		return clorofila;
	}
	
	public void setClorofila(double clorofila) {
		this.clorofila = clorofila;
	}
	
	public String getFormula() {
		return formula;
	}
	
	public void setFormula(String formula) {
		this.formula = formula;
	}
	
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getHora() {
		return hora;
	}
	
	public void setHora(String hora) {
		this.hora = hora;
	}
	
	public double getLatitud() {
		return latitud;
	}
	
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	
	public double getLongitud() {
		return longitud;
	}
	
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}	
	
}
