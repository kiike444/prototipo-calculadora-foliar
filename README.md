# Foliar Android#

Prototipo de una aplicación móvil que calcula el área foliar y el índice de clorofila de las hojas de las plantas.

* Versión 2.0

### Dependencias ###

* OpenCV para Android (http://opencv.org/)

### Requerimientos de los dispositivos ###

* Android 2.3.3 o superior
* Procesador ARMv7